%global __requires_exclude libfolks\(\)|libfolks-dummy\(\)|libfolks-eds\(\)|libfolks-telepathy\(\)

%global folks_module_version 26

Name:           folks
Epoch:          1
Version:        0.15.9
Release:        1
Summary:        Library aggregates people from multiple sources
License:        LGPL-2.1-or-later
URL:            https://wiki.gnome.org/Projects/Folks
Source0:        https://download.gnome.org/sources/folks/0.15/folks-%{version}.tar.xz
Patch0:         folks-eds-test-timeout.patch
Patch1:         folks-disable-bluez-check.patch
Patch2:	        folks-disable-some-check-for-obs.patch

BuildRequires:  gcc meson intltool gettext libxml2-devel GConf2-devel
BuildRequires:  pkgconfig(dbus-glib-1) evolution-data-server-devel >= 3.33.2 pkgconfig(gee-0.8) >= 0.8.4
BuildRequires:  glib2-devel gobject-introspection-devel 
BuildRequires:  python3-dbusmock python3-devel readline-devel
BuildRequires:  telepathy-glib-devel telepathy-glib-vala vala vala-devel >= 0.17.6
BuildRequires:  pkgconfig(gee-0.8) >= 0.8.4

%description
libfolks is a library that aggregates people from multiple sources
(eg, Telepathy connection managers) to create metacontacts.

%package        telepathy
Summary:        Folks telepathy backend
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description    telepathy
%{name}-telepathy contains the folks telepathy backend.

%package        tools
Summary:        Tools for %{name}
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description    tools
%{name}-tools contains a database and import tool.

%package        devel
Summary:        Development files for folks
Requires:       %{name} = %{epoch}:%{version}-%{release}
Requires:       %{name}-tools = %{epoch}:%{version}-%{release}

%description    devel
This package contains libraries and header files.

%prep
%autosetup -p1

%build
%meson
%meson_build

%install
%meson_install

%find_lang %{name}

%files -f %{name}.lang
%license COPYING
%doc AUTHORS NEWS
%{_libdir}/libfolks-dummy.so.26*
%{_libdir}/libfolks-eds.so.26*
%{_libdir}/libfolks.so.26*
%dir %{_libdir}/folks
%dir %{_libdir}/folks/%{folks_module_version}
%dir %{_libdir}/folks/%{folks_module_version}/backends
%{_libdir}/folks/%{folks_module_version}/backends/bluez/
%{_libdir}/folks/%{folks_module_version}/backends/dummy/
%{_libdir}/folks/%{folks_module_version}/backends/eds/
%{_libdir}/folks/%{folks_module_version}/backends/key-file/
%{_libdir}/folks/%{folks_module_version}/backends/ofono/
%{_libdir}/girepository-1.0/Folks-0.7.typelib
%{_libdir}/girepository-1.0/FolksDummy-0.7.typelib
%{_libdir}/girepository-1.0/FolksEds-0.7.typelib
%{_datadir}/GConf/gsettings/folks.convert
%{_datadir}/glib-2.0/schemas/org.freedesktop.folks.gschema.xml

%files telepathy
%{_libdir}/libfolks-telepathy.so.26*
%{_libdir}/folks/%{folks_module_version}/backends/telepathy
%{_libdir}/girepository-1.0/FolksTelepathy-0.7.typelib

%files tools
%{_bindir}/%{name}-import
%{_bindir}/%{name}-inspect

%files devel
%{_includedir}/folks
%{_libdir}/*.so
%{_libdir}/pkgconfig/folks*.pc
%{_datadir}/gir-1.0/Folks-0.7.gir
%{_datadir}/gir-1.0/FolksDummy-0.7.gir
%{_datadir}/gir-1.0/FolksEds-0.7.gir
%{_datadir}/gir-1.0/FolksTelepathy-0.7.gir
%dir %{_datadir}/vala
%dir %{_datadir}/vala/vapi
%{_datadir}/vala/vapi/%{name}*

%changelog
* Tue Feb 18 2025 Funda Wang <fundawang@yeah.net> - 1:0.15.9-1
- update to 0.15.9

* Fri Nov 24 2023 zhangxianting <zhangxianting@uniontech.com> - 1:0.15.6-1
- update to version 0.15.6

* Thu Aug 11 2022 wenlong ding <wenlong.ding@turbolinux.com.cn> - 1:0.15.5-3
- Disable %check for OBS building error.

* Mon Jul 18 2022 wenlong ding <wenlong.ding@turbolinux.com.cn> - 1:0.15.5-2
- Add folks-disable-some-check-for-obs.patch fix obs check error.

* Mon Jun 13 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 1:0.15.5-1
- Update to 0.15.5

* Mon Jun 28 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 3.38.3-1
- Get rid of repeated build dependency that may cause fault

* Fri Jun 25 2021 Wenlong Ding <wenlong.ding@turbolinux.com.cn> - 1:0.15.2-1
- Update to 1:0.15.2

* Wed Dec 25 2019 Ling Yang <lingyang2@huawei.com> - 1:0.11.4-10
- Add epoch for providing folks-tools

* Sat Dec 21 2019 Senlin Xia <xiasenlin1@huawei.com> - 1:0.11.4-9
- Package init
